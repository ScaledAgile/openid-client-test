// imports
const express = require('express');
const open = require('open');
const OAuthClientFactory = require('./oauth');

// configuration - these will probably be different in each environment
const oauthConfig = {
    base_url: process.env.OAUTH_TEST_BASE_URL,
    redirect_uri: process.env.OAUTH_TEST_REDIRECT_URI,
    client_id: process.env.OAUTH_TEST_CLIENT_ID,
    client_secret: process.env.OAUTH_TEST_CLIENT_SECRET,
    debug: process.env.OAUTH_TEST_DEBUG === 'true'
};

// oauth endpoints
const oauthClientFactory = new OAuthClientFactory(oauthConfig);

const oauthSessions = {};


// express web server - responds to redirect callback
const app = express();

app.get('/', (req,res) => {
    let html = '<html><body><div><a href="/new">Click Here</a> to login</div></body></html>';
    res.send(html);
})

app.get('/new', (req,res) => {
    const oauthClient = oauthClientFactory.createNewAuthorizeSession();
    oauthSessions[oauthClient.session.id] = oauthClient;
    let authorizeLink = oauthClient.buildAuthorizeLink();
    res.redirect(authorizeLink);
});

// setup callback route to respond to redirect_uri
app.get('/cb', async(req,res) => {
    try {
        if (oauthConfig.debug) {
            console.log('callback:', req.query, '\n');
        }
        // check for error
        if (req.query.error) {
            throw `${req.query.error}: ${req.query.error_description}`;
        }

        // get authorization code from query string
        let code = req.query.code;
        let authSessionId = req.query.state;
        const oauthClient = oauthSessions[authSessionId];
        delete oauthSessions[authSessionId];

        let tokenResp = await oauthClient.fetchAccessToken(code);
        let userInfo = await oauthClient.fetchUserInfo();
        // send userinfo response to browser
        let responseHtml = '<html><body><pre>' + JSON.stringify(userInfo, null ,2) + '</pre></body></html>';
        res.send(responseHtml);
    } catch (err) {
        res.send(err);
    }
});

// start web server on port 3000 and open chrome
app.listen(3000, () => {
    open('http://localhost:3000/', { app: ['google chrome', '--incognito'] });
});