# Salesforce login using OpenID Connect
This demo app uses the Web Server Flow detailed here: https://help.salesforce.com/articleView?id=remoteaccess_oauth_web_server_flow.htm&type=5

## Running the Demo App
The demo app will execute an OAuth login flow. It can be used as a reference app to detail how the various operations work. We will go into more details about how the flow works later.

### Setup this app
1. Install NodeJS
2. Clone this repo
3. use `npm install` to install add required modules

### Setup Connected App
1. Create an Connected App in your Salesforce org. In Setup, go to Apps > App Manager and click the New Connected App button
2. Provide the required fields in Basic Info section.
3. In the API section, check the Enable OAuth checkbox. Provide a Callback URL for your app. (This app uses http://localhost:3000/cb)
4. Under the Selected OAuth Scopes field, add the _openid_ scope
5. Leave the rest as is and click Save to create the connected app.
6. Once the connected app is created, copy the Redirect URI, Consumer Key and Consumer Secret.
7. Create a .env file in this project with the following:
```
OAUTH_TEST_BASE_URL=<Salesforce Community URL eg. https://sso-scaledagile.cs95.force.com/community>
OAUTH_TEST_REDIRECT_URI=<Callback URL>
OAUTH_TEST_CLIENT_ID=<Consumer Key>
OAUTH_TEST_CLIENT_SECRET=<Consumer Secret>
OAUTH_TEST_DEBUG=true
```

### Run this app to test authentication
1. use `npm start` to start the app. This will start the local web server and open a Chome window.
2. Click the link to start the OAuth authorization flow
3. Authenticate using your community user's username and password
4. Click Allow to allow this app to access your session. You only need to do this once.
5. If everything went well you should see a huge block of JSON on the page. This is the output of the UserInfo endpoint, and it signals that authentication succeeded.

## Breakdown of the OAuth Web Server Flow

The OAuth Web Server Flow allows a user to authenticate and authorize a third-party app to use their Salesforce session. Different levels of access to Salesforce are granted by OAuth scopes configured in the Connected App. We will only be covering the openid scope, which allows a user to authenticate and retrieve their own user data from the UserInfo endpoint.

### Connected App
In order to begin the web server flow, you will need to create a Connected App in Salesforce and retrieve the following info:
* Community URL - This will be different for every Sandbox, and it will change when Sandboxes are refreshed. Example: https://sso-scaledagile.cs95.force.com/community. It will always be https://community.scaledagile.com for production.
* Consumer Key - get this from the Connected App page
* Consumer Secret - get this from the Connected App page
* Callback URL - Configured in the Connected App page. Salesforce will redirect the user's browser to this page after they authorize the app to access their data. Example: http://localhost:3000/cb

All examples below will use the following values:
* Community URL: https://sso-scaledagile.cs95.force.com/community
* Callback URL: http://localhost:3000/cb
* Consumer Key:<client_id>
* Consumer Secret:<client_secret>

### Step 1 - Authorization
Authorization is done by crafting an authorize URL and redirecting the user's browser to that URL. Salesforce will then prompt the user to login with their community user's credentials and allow the app to access their data. Afterwards the user's browser will be redirected to the Callback URL to exchange the authorization code for an access token.

The Authorize URL needs the following query parameters:
* grant_type - use `authorization_code`
* response_type - use `code`
* client_id - the Consumer Key of the Connected App
* redirect_uri - the Callback URL of the Connected App
* nonce - optional, used eith openid for id_token validation to prevent replay attacks. Randmonly generated number with 13 digits (like a unix timestamp). Must be unique per authorize request.
* code_challenge - SHA256 hash value of the code_verifier value in the token request to help prevent authorization code interception attacks. This parameter is required only if a code_verifier parameter is specified in the token request. We will discuss this in more detail below.
* state - (Optional) Any state that the consumer wants to have sent back to the callback URL. Useful for associating a callback request to a specific authorize session.

Example Authorize URL:
https://sso-scaledagile.cs95.force.com/community/services/oauth2/authorize?grant_type=authorization_code&response_type=code&client_id=<client_id>&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Fcb&nonce=6551164739770&code_challenge=cXXAbn1HIQVb4BV6eiX4vGv4FEtdWcMEqs0PzB-71rA&state=abad900ffff275531a143c9422aff19f

### Step 2 - Retrieve Authorization Code from Callback
The callback URL will have several query parameters passed to it. The important one is called `code`. This is the authorization code you need to use to get an access token.

If the authorization step failed for any reason, the callback will have query parameters of `error` with the error code and `error_description` with a brief explanation of the failure.

Query parameters on successful authorization:
* code: <authorization_code>,
* state: 'abad900ffff275531a143c9422aff19f',
* sfdc_community_url: 'https://sso-scaledagile.cs95.force.com/community',
* sfdc_community_id: '0DBd0000000PBBPGA4'

Query parameters on failed authorization:
* error:
* error_description:

Example:
https://localhost:3000/cb?code=<authorization_code>&state=abad900ffff275531a143c9422aff19f

### Step 3 - Request Access Token with Authorization Code
To get an access token the server needs to POST to the token endpoint with the authorization code retrieved from the Callback URL. Salesforce will respond with an access_token, which is used to authenticate all future Salesforce requests for this session, and an id_token, which is a JWT that can be used to verify the authenticity of the response.

Note that this step deals with secrets so it must be performed at the server. Do not send the token link to the client's browser. Do not send the client_secret to the client's browser.

The Token URL must use the POST method. The body should contain content tthat is application/x-www-form-urlencoded.
Form Data:
* grant_type - use `authorization_code`
* code - use the authorization code passed to the callback url
* client_id - <client_id>
* client_secret - <client_secret>
* redirect_uri - same redirect uri passed to authorize endpoint
* code_verifier - 128 bytes of random data with high enough entropy to make it difficult to guess the value. This entropy helps prevent authorization code interception attacks. This parameter is required only if a code_challenge parameter was specified in the authorization request. We will discuss this in more detail below.

If the token request succeeds you will get back a JSON object with many fields. The following are the most important:
* id - used as the subject in id_token verification
* access_token - this is your access token that is used to authenticate all requests to Salesforce
* id_token - openid JWT token that we will verify in the next step

If the token request fails you will get back a JSON object with these fields:
* error - code of the error
* error_description - brief description of the error

Example Token request:

POST https://sso-scaledagile.cs95.force.com/community/services/oauth2/token


grant_type=authorization_code&
code=aPrxdxD8w1fITt_kyxeeelDLYbBCcgUKnEeTtS9.Ri0lmODSP6kGCwlxwvNmPd9LFzgmz2SeYg==&
client_id=<client_id>&
client_secret=<client_secret>&
redirect_uri=http://localhost:3000/cb&
code_verifier=8bc95a7e2b298774f5fed1b6be8bcd96a84ba0bd07997fbf17e23787fe34caa1

### Step 4 - Validate ID Token
This step is specific to the OpenID Connect protocol. It is used to add an extra layer of security over the authorization process and prove the access_token was legitimatly created by Salesforce. It is highly recommended to use a thrid-party library to perform the verification.

Here are the steps a third party lib will perform:
1. Decode id_token. It is a JWT token. JWT.io is a good resource for working with JWT tokens. The token is composed of three parts: header, payload and signature.
2. Retrieve the public keys from the JWKS endpoint. This endpoint will be http://<community_url>/id/keys. Example: https://sso-scaledagile.cs95.force.com/community/id/keys
3. Retrieve the kid from the token header and find the correct key from the public keys retrieved above.
4. Create an RSA public key from the data above and use it to verify the JWT signature.
5. Verify the payload data.
    1. iss - should match <community_url>
    2. sub - should match the id of the token response in step 3
    3. aud - should match the <client_id>
    4. nonce - should match the nonce provided in the authorize step 1
    5. exp - this is a unix timestamp of the expiration date/time. use it to make sure the token has not expired

Now that the id_token has been verified we can trust the access_token.

### Step 5 - Get UserInfo with Access Token
The userinfo endpoint contains basic information about the authenticated user and also any custom data. For example we can add custom data to represent the SAFe Certifications the authenticated user has earned.

In order to access the userinfo endpoint we will need to use the Access Token to authorize our request. We do this by adding a Authorization header to the request:

`Authorization: Bearer <access_token>`

Example Request:

POST https://sso-scaledagile.cs95.force.com/community/services/oauth/userinfo

Headers

`Authorization: Bearer <access_token>`


Example Response:

```json
{
  "sub": "https://test.salesforce.com/id/00D0x0000000rTIEAY/0050x000002ebnRAAQ",
  "user_id": "0050x000002ebnRAAQ",
  "organization_id": "00D0x0000000rTIEAY",
  "preferred_username": "user.name@example.com",
  "nickname": "qoiudmng38d",
  "name": "firstname lastname",
  "email": "user.email@example.com",
  "email_verified": true,
  "given_name": "firstname",
  "family_name": "lastname",
  "zoneinfo": "America/Denver",
  "photos": {
    "picture": "https://sso-scaledagile.cs95.force.com/img/userprofile/default_profile_200_v2.png",
    "thumbnail": "https://sso-scaledagile.cs95.force.com/img/userprofile/default_profile_45_v2.png"
  },
  "profile": "https://scaledagile--sso.cs95.my.salesforce.com/0050x000002ebnRAAQ",
  "picture": "https://sso-scaledagile.cs95.force.com/img/userprofile/default_profile_200_v2.png",
  "address": {},
  "urls": {
    "enterprise": "https://scaledagile--sso.cs95.my.salesforce.com/community/services/Soap/c/{version}/00D0x0000000rTI",
    "metadata": "https://scaledagile--sso.cs95.my.salesforce.com/community/services/Soap/m/{version}/00D0x0000000rTI",
    "partner": "https://scaledagile--sso.cs95.my.salesforce.com/community/services/Soap/u/{version}/00D0x0000000rTI",
    "rest": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/",
    "sobjects": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/sobjects/",
    "search": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/search/",
    "query": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/query/",
    "recent": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/recent/",
    "tooling_soap": "https://scaledagile--sso.cs95.my.salesforce.com/community/services/Soap/T/{version}/00D0x0000000rTI",
    "tooling_rest": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/tooling/",
    "profile": "https://scaledagile--sso.cs95.my.salesforce.com/0050x000002ebnRAAQ",
    "feeds": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/chatter/feeds",
    "groups": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/chatter/groups",
    "users": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/chatter/users",
    "feed_items": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/chatter/feed-items",
    "feed_elements": "https://scaledagile--sso.cs95.my.salesforce.com/services/data/v{version}/chatter/feed-elements",
    "custom_domain": "https://scaledagile--sso.cs95.my.salesforce.com"
  },
  "active": true,
  "user_type": "CSP_LITE_PORTAL",
  "language": "en_US",
  "locale": "en_US",
  "utcOffset": -25200000,
  "updated_at": "2019-07-12T14:09:11Z",
  "is_app_installed": true,
  "custom_attributes": {
    "Active Certifications": "POPM;RTE;SA;SASM;SCPT4;SPC4;SSM",
    "Expiration Date": "2020-07-11"
  }
}
```

## How the Code Verifier/Code Challenge works

The code_verifier and code_challenge add an extra layer of security to the web server flow that helps prevent attacks that might intercept the authorization code passed to the callback url. Both of these values are created from a single value that we will call the verifier. The verifier should be generated randomly for every authorization session, and it should be remembered (along with the nonce) so we can reuse it when requesting an access token. Generation of the verifier, code_verifier and code_challenge are highly specific to your programming language, and I will provide an example using node javascript.

```js
// import crypto library
const crypto = require('crypto');

// create a verifier from random bytes - length is important, also all characters should be ascii
const verifier = crypto.randomBytes(32).toString('hex');

// create code_challenge for authorization step
// the code_challenge is a SHA256 hash of the verifier
// value must be base64 encoded with a URL-compatible encoding
const code_challenge = base64URLEncode(crypto.createHash('sha256').update(verifier).digest());

// encode code_verifier for request access token step
// value must be base64 encoded with a URL-compatible encoding
const code_verifier = base64URLEncode(verifier);

// URL-compatible base64 encoding
function base64URLEncode(str) {
    return str.toString('base64') // base64
        .replace(/\+/g, '-') // replace + with -
        .replace(/\//g, '_') // replace / with _
        .replace(/\=/g, ''); // remove =
}

```