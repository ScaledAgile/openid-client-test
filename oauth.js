const crypto = require('crypto');
const fetch = require('node-fetch');
const jwksClient = require('jwks-rsa');
const jwt = require('jsonwebtoken');

const OAUTH_PATH = 'services/oauth2',
      AUTHORIZE_EP = 'authorize',
      TOKEN_EP = 'token',
      USERINFO_EP = 'userinfo',
      JWKS_EP = 'id/keys';


class OAuthClientFactory {
    constructor(oauthConfig) {
        this.config = oauthConfig;
    }

    createNewAuthorizeSession() {
        return new OAuthClient(this.config);
    }
}

class OAuthClient {
    constructor(oauthConfig) {
        this.base_url = oauthConfig.base_url;
        this.redirect_uri = oauthConfig.redirect_uri;
        this.client_id = oauthConfig.client_id;
        this.client_secret = oauthConfig.client_secret;
        this.debug = oauthConfig.debug;
        this.session = {
            id: crypto.randomBytes(16).toString('hex'),
            verifier: crypto.randomBytes(32).toString('hex'),
            nonce: generateNonce(),
        };
    }

    get authUrl() {
        return `${this.base_url}/${OAUTH_PATH}/${AUTHORIZE_EP}`;
    }

    get tokenUrl() {
        return `${this.base_url}/${OAUTH_PATH}/${TOKEN_EP}`;
    }

    get userinfoUrl() {
        return `${this.base_url}/${OAUTH_PATH}/${USERINFO_EP}`;
    }

    buildAuthorizeLink() {
        let authorize_link = this.authUrl;
        authorize_link += '?grant_type=authorization_code';
        authorize_link += '&response_type=code';
        authorize_link += '&client_id=' + encodeURIComponent(this.client_id);
        authorize_link += '&redirect_uri=' + encodeURIComponent(this.redirect_uri);
        authorize_link += '&nonce=' + encodeURIComponent(this.session.nonce);
        let code_challenge = base64URLEncode(crypto.createHash('sha256').update(this.session.verifier).digest());
        authorize_link += '&code_challenge=' + code_challenge;
        authorize_link += '&state=' + this.session.id;
        // authorize_link += '&scope=openid';

        if (this.debug) {
            console.log('authorize_link:', authorize_link, '\n');
        }
        return authorize_link;
    }

    fetchAccessToken(authCode) {
        const tokenParams = new URLSearchParams();
        tokenParams.set('grant_type', 'authorization_code');
        tokenParams.set('code', authCode);
        tokenParams.set('client_id', this.client_id);
        tokenParams.set('client_secret', this.client_secret);
        tokenParams.set('redirect_uri', this.redirect_uri);
        let code_verifier = base64URLEncode(this.session.verifier);
        tokenParams.set('code_verifier', code_verifier);

        if (this.debug) {
            console.log('tokenParams:', tokenParams, '\n');
        }

        return fetch(this.tokenUrl, { 
            method: 'POST', 
            body: tokenParams
        })
        .then(res => {
            if (!res.status === 200) {
                console.log(`${res.status} : ${res.statusText}`);
            }
            return res;
        })
        .then(res => res.json())
        .then(tokenResp => {
            if (this.debug) {
                console.log('tokenResp:', tokenResp, '\n');
            }
            // check for error
            if (tokenResp.error) {
                throw tokenResp.error + ': ' + tokenResp.error_description;
            }
            return this.validateToken(tokenResp);
        })
        .then(tokenResp => {
            this.session.access_token = tokenResp.access_token;
            return tokenResp;
        });
    }

    async validateToken(tokenResp) {
        let id_token = tokenResp.id_token;

        const client = jwksClient({
            jwksUri: `${this.base_url}/${JWKS_EP}`,
        });
        function getKey(header, callback){
            client.getSigningKey(header.kid, function(err, key) {
                var signingKey = key.publicKey || key.rsaPublicKey;
                callback(null, signingKey);
            });
        }

        await new Promise((resolve, reject) => {
            jwt.verify(id_token, getKey, {
                issuer: tokenResp.sfdc_community_url,
                subject: tokenResp.id,
                nonce: this.session.nonce.toString(),
                audience: this.client_id,
                complete: true
            }, (err, decoded) => {
                if (err) {
                    reject(err);
                }
                else {
                    if (this.debug) {
                        console.log('id_token:', decoded, '\n');
                    }
                    resolve(decoded);
                }
            });
        });
        return tokenResp;
    }

    fetchUserInfo() {
        // fetch userinfo using access token
        return fetch(this.userinfoUrl, { 
            method: 'POST', 
            headers: { 
                'Authorization': `Bearer ${this.session.access_token}`
            }
        })
        .then(res => res.json())
        .then(res => {
            if (this.debug) {
                console.log('userInfo:', res, '\n');
            }
            return res;
        });
    }
}

function base64URLEncode(str) {
    return str.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/\=/g, '');
}

const nonceMin = 1000000000000,
    nonceMax = 9999999999999;
function generateNonce() {
    return Math.floor(Math.random() * (nonceMax - nonceMin)) + nonceMin;
}

module.exports = OAuthClientFactory;